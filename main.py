from PyQt5.QtWidgets import QApplication
import sys
from pyads import ADSError

from twinpy.twincat import SimulinkModel
from twinpy.twincat import TwincatConnection

from ui.gui import QuasimodoGUI


def main():

    app = QApplication(sys.argv)

    models = {
        "actuator": SimulinkModel(0x01010010, "QuasimodoActuators"),
        "controller": SimulinkModel(0x01010020, "QuasimodoController"),
    }

    try:
        connection = TwincatConnection()
        for name, model in models.items():
            model.connect_to_twincat(connection)

    except ADSError as err:
        connection = None
        print("Not connected to TwinCAT, continuing -", err)

    gui = QuasimodoGUI(**models)

    app.exec()


if __name__ == "__main__":
    main()
