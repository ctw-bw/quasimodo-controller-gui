from PyQt5.QtWidgets import QWidget, QGroupBox, QVBoxLayout, QFormLayout, QLabel

from twinpy.ui import TcRadioButtonGroupBox, TcLineEdit, TcLabel, TcCheckBox


class ControllerTab(QWidget):
    """Tab giving access to controller settings."""

    def __init__(self, controller):
        super().__init__()

        layout_main = QVBoxLayout(self)

        # Control mode

        self.radio_mode = TcRadioButtonGroupBox(
            "Mode",
            options=[
                ("Minimal Impedance", 0),
                ("Position Control", 1),
                ("Constant Torque", 2),
            ],
            symbol=controller.Settings.Mode.Value,
        )
        layout_main.addWidget(self.radio_mode)

        # Settings

        settings = [
            ("Max. Torque Lumbar", controller.Settings.MaxTorque_Lumbar.Value),
            ("Max. Torque Hip", controller.Settings.MaxTorque_Hip.Value),
            ("ROM Min. Lumbar", controller.Settings.ROM_Min_Lumbar.Value),
            ("ROM Max. Lumbar", controller.Settings.ROM_Max_Lumbar.Value),
            ("ROM Min. Hip", controller.Settings.ROM_Min_Hip.Value),
            ("ROM Max. Hip", controller.Settings.ROM_Max_Hip.Value),
            ("Max. Ref. Vel.", controller.Settings.MaxRefVel.Value),
            ("Kp", controller.Settings.Kp.Value),
            ("Kd", controller.Settings.Kd.Value),
        ]

        group_settings = QGroupBox("Settings")
        group_settings_layout = QFormLayout(group_settings)

        self.input_settings = []

        for label, symbol in settings:
            line_edit = TcLineEdit(symbol=symbol)
            group_settings_layout.addRow(QLabel(label), line_edit)
            self.input_settings.append(line_edit)

        layout_main.addWidget(group_settings)

        group_target = QGroupBox("Target")
        group_target_layout = QFormLayout(group_target)

        self.input_target_lumbar = TcLineEdit(symbol=controller.PositionControl.TargetPos_LumbarFlexion.Value)
        self.input_target_hip = TcLineEdit(symbol=controller.PositionControl.TargetPos_HipFlexion.Value)
        group_target_layout.addRow(QLabel("Lumbar Position:"), self.input_target_lumbar)
        group_target_layout.addRow(QLabel("Hip Position:"), self.input_target_hip)

        layout_main.addWidget(group_target)


class ActuatorTab(QWidget):
    """Tab with some basic joint info."""

    JOINTS = ["LLF", "LHF", "RLF", "RHF"]

    def __init__(self, controller, actuator):
        super().__init__()

        layout_main = QVBoxLayout(self)

        for joint in self.JOINTS:
            checkbox = TcCheckBox(joint)

            try:
                block = getattr(controller.UseActuators, joint)
                checkbox.connect_symbol(block.Value)
                self.connected = True
            except AttributeError:
                self.connected = False

            groupbox = QGroupBox(joint)
            groupbox_layout = QFormLayout(groupbox)

            from_act = getattr(actuator.FromActuators, joint)

            groupbox_layout.addRow(
                QLabel("Joint Angle"),
                TcLabel(symbol=from_act.ToJointAngle_rad.so1)
            )

            groupbox_layout.addRow(
                QLabel("Joint Torque"),
                TcLabel(symbol=from_act.ToJointTorque.so2)
            )

            groupbox_layout.addWidget(checkbox)
            layout_main.addWidget(groupbox)
