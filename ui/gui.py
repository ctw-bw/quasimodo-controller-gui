from typing import Optional

from twinpy.twincat import SimulinkModel
from twinpy.ui import TcWidget, BaseGUI, ErrorPopupWindow

from .tabs import ControllerTab, ActuatorTab


ErrorPopupWindow.JOINTS = ["LLF", "LHF", "RLF", "RHF"]


TcWidget.DEFAULT_EVENT_TYPE = TcWidget.EVENT_TIMER


class QuasimodoGUI(BaseGUI):
    """GUI for the Quasimodo.

    GUI has basic drive controls and cna change parameters for the high-level
    controller.
    """

    def __init__(
        self,
        actuator: Optional[SimulinkModel] = None,
        controller: Optional[SimulinkModel] = None,
        **kwargs
    ):

        if controller is not None and hasattr(controller, "Controller"):
            controller = controller.Controller  # Shorten one block

        super().__init__(actuator, **kwargs)

        # Fix console
        self.tab_console.push_local_ns("actuator", actuator)
        self.tab_console.push_local_ns("controller", controller)

        # Add tabs
        self.tab_actuator = ActuatorTab(controller, actuator)
        self.tabs.addTab(self.tab_actuator, "Joints")

        self.tab_controller = ControllerTab(controller)
        self.tabs.addTab(self.tab_controller, "Controller")

        self.tabs.setCurrentWidget(self.tab_controller)
